# DPI
PRODUCT_PROPERTY_OVERRIDES += \
    ro.sf.lcd_density=213

# Fix for echo in calls
PRODUCT_PROPERTY_OVERRIDES += \
    audio_hal.disable_two_mic=false
